# YS Reset #

A CSS Reset based on Eric Meyer's Reset CSS v2.0 - http://cssreset.com

Essentially Eric Meyer's Reset modernized to HTML5 standards, with obsolete tags removed and missing HTML5 tags added.
Also default to `box-sizing: border-box`

Also contains minor fixes targeting popular browsers, e.g.
[Firefox buttons have extra padding](http://stackoverflow.com/questions/5517744/remove-extra-button-spacing-padding-in-firefox)
[Webkit details does not inherit box-sizing correctly](https://bugs.chromium.org/p/chromium/issues/detail?id=589475)

### Browser Support ###
* IE 11+
* Chrome
* Edge
* Firefox
* Opera
* Safari

### Set Up ###

1. Download the latest release (as of 28 Apr 2019, v2.0).
2. Include in your HTML (inside `<head>`):

    `<link rel="stylesheet" href="/path/to/reset.css">`
    
3. Start styling.

### FAQ ###

1. What's wrong with Eric Meyer's Reset?
> Nothing. For a long time, we had been using Eric Meyer's CSS Reset. But we always needed to add a few lines of additional defaults and "browser fixes" to normalise things across browsers. Also, while looking through the code of Eric Meyer's Reset, we notice a few tags are uncessary / missing. Notably,  So we created our own CSS reset based off Eric Meyer's reset for use in our projects.
>
> If you are 100% used to Eric Meyer's Reset (or another reset/normalize CSS), i.e. you depend on weird anomalies like Firefox having extra button padding; or if you prefer not to work with `box-sizing: border-box`, then this CSS Reset is probably not for  you.

1. Where are your vendor prefixes?
> Vendor prefixes are pretty much unnecessary today. Since they are only necessary for legacy browsers, vendor prefixes only add unnecessary bytes to the load for 99.9% of users who use modern browsers.
>
> We should all adopt an evergreen-browser suppport system, and stop supporting ancient browsers. Hopefully we can banish legacy browsers from existence for good.

### Contact ###

* Email us at <yokestudio@hotmail.com>.